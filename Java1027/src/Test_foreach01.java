
public class Test_foreach01 {

	public static void main(String[] args) {

		int[] array = new int[10];
		
		for(int i = 0; i<array.length; i++){
			array[i]=i+1;
		}
		
		// for - each ��
		for(int i : array){
			System.out.print(i+" ");
		}
//		for(int i = 0; i<array.length; i++){
//			System.out.print(array[i]+ " ");
//		}
		
		String[] s = {"aa","bb","cc","dd"};
		for(String ss : s){
			System.out.println(ss);
		}
	}

}
