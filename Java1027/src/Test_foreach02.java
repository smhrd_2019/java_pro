
public class Test_foreach02 {

	public static void main(String[] args) {

		int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int count = 0;

		System.out.print("arr에 들어 있는 홀수는 ");
		for (int i : arr) {
			if (i % 2 == 1) {
				System.out.print(i + " ");
				count++;
			}
		}
		System.out.print("이며 총 " + count + "개 입니다.");
	}
}
