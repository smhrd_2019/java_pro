import java.util.Scanner;

public class Test_SequentialSearch {

	public static void main(String[] args) {

		// 길이가 길어지면 비효율적이니 binary search로 바꿔준다.
		
		int[] arr = { 13, 35, 15, 11, 26, 72, 78, 13, 61, 90 };
		Scanner sc = new Scanner(System.in);

		System.out.print("찾는 수를 입력 >> ");
		int num = sc.nextInt();

		// for (int i = 0; i < arr.length; i++) {
		// if (arr[i] == num) {
		// System.out.print("찾는 수는 " + (i + 1) + "번째에 있습니다.");
		//
		// }
		// }
		int count = 0;
		for (int i : arr) {
			count++;
			if (i == num) {
				System.out.print("찾는 수는 " + count + "번째에 있습니다.");
				break;
			}
		}
	}

}
