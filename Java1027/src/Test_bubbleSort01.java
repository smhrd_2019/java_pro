
public class Test_bubbleSort01 {

	public static void main(String[] args) {

		int[] arr = { 30, 24, 1, 88, 77 };

		int num = 0;
		for (int j = 0; j < arr.length; j++) {
			for (int i = 1; i < arr.length-j; i++) {
				if (arr[i - 1] > arr[i]) {

					num = arr[i - 1];
					arr[i - 1] = arr[i];
					arr[i] = num;
				}

			}
		}
		for (int i : arr) {
			System.out.println(i);
		}
	}

}
