
public class Test_binarySearch01 {

	public static void main(String[] args) {
		// 히히
		// 정렬이 되어 있을 때 사용 가능

		int[] arr = { 1, 7, 16, 25, 30, 33, 41, 66, 78, 90 };
		int lowIndex = 0;
		int highIndex = arr.length - 1;
		int num = 78;

		do {

			int midIndex = (lowIndex + highIndex) / 2;
			if (arr[midIndex] == num) {
				System.out.println(num + "은 " + midIndex + "번째 있습니다.");
				break;

			} else if (arr[midIndex] > num) {
				highIndex = midIndex - 1;
			} else if (arr[midIndex] < num) {
				lowIndex = midIndex + 1;
			}

		} while (true);
	}

}
